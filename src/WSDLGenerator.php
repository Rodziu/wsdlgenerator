<?php
/*
 *      ______                                   __
 *  __ / / __/______ ___ _  ___ _    _____  ____/ /__
 * / // / _// __/ _ `/  ' \/ -_) |/|/ / _ \/ __/  '_/
 * \___/_/ /_/  \_,_/_/_/_/\__/|__,__/\___/_/ /_/\_\
 *
 * @author Rodziu <mateusz.rohde@widzialni.pl>
 *
 */
namespace JFramework;
/**
 * Class WSDLGenerator
 *
 * @version 1.0.1
 * @author Rodziu <mateusz.rohde@gmail.com>
 */
class WSDLGenerator{
	/**
	 * @var string
	 */
	private $namespace = "";
	/**
	 * @var string
	 */
	private $url = "";
	/**
	 * @var \DOMDocument
	 */
	private $xml;
	/**
	 * @var \DOMElement
	 */
	private $definitions;
	/**
	 * @var \DOMElement
	 */
	private $schema;
	/**
	 * @var \DOMElement
	 */
	private $port;
	/**
	 * @var \DOMElement
	 */
	private $binding;

	/**
	 * WSDLGenerator constructor.
	 *
	 * @param $namespace
	 * @param $url
	 */
	public function __construct($namespace, $url){
		$this->namespace = $namespace;
		$this->url = $url;
		$this->xml = new \DOMDocument();
		$this->definitions = $this->createElement('definitions', [
			'targetNamespace' => "urn:$this->namespace",
			'xmlns'           => 'http://schemas.xmlsoap.org/wsdl/',
			'xmlns:soap'      => 'http://schemas.xmlsoap.org/wsdl/soap/',
			'xmlns:tns'       => "urn:$this->namespace",
			'xmlns:xsd'       => 'http://www.w3.org/2001/XMLSchema',
			'xmlns:SOAP-ENC'  => 'http://schemas.xmlsoap.org/soap/encoding/',
			'xmlns:wsdl'      => 'http://schemas.xmlsoap.org/wsdl/'
		]);
		$this->xml->appendChild($this->definitions);
		// Import Schemas
		$this->schema = $this->createElement('xsd:schema', ['targetNamespace' => "urn:$this->namespace"]);
		$this->schema->appendChild($this->createElement('xsd:import', ['namespace' => "http://schemas.xmlsoap.org/soap/encoding/"]));
		$this->schema->appendChild($this->createElement('xsd:import', ['namespace' => "http://schemas.xmlsoap.org/wsdl/"]));
		$this->definitions->appendChild($this->createElement('types'))->appendChild($this->schema);
		// Operations
		$this->port = $this->createElement('portType', ['name' => $this->namespace."Operations"]);
		$this->binding = $this->createElement('binding', ['name' => $this->namespace.'Binding', 'type' => $this->namespace."Operations"]);
		$this->binding->appendChild($this->createElement('soap:binding', ['style' => 'rpc', 'transport' => 'http://schemas.xmlsoap.org/soap/http']));
	}

	/**
	 * @param string $name
	 * @param array $attributes
	 *
	 * @return \DOMElement
	 */
	private function createElement($name, array $attributes = []){
		$element = $this->xml->createElement($name);
		foreach($attributes as $a => $v){
			$element->setAttribute($a, $v);
		}
		return $element;
	}

	/**
	 * @param string $type - array or struct
	 * @param string $name - complexType name
	 * @param string|array $xsdType - type of array or array of ['parameterName' => 'type'] for struct
	 *
	 * @throws \ErrorException
	 */
	public function addComplexType($type, $name, $xsdType){
		$complexType = $this->createElement('xsd:complexType', ['name' => $name]);
		if($type == 'array'){
			$complexType->appendChild($this->createElement('xsd:complexContent'))
				->appendChild($this->createElement('xsd:restriction', ['base' => 'SOAP-ENC:Array']))
				->appendChild($this->createElement('xsd:attribute', ['ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => $xsdType.'[]']));
		}else if($type == 'struct'){
			$all = $complexType->appendChild($this->createElement('xsd:all'));
			foreach($xsdType as $k => $v){
				$all->appendChild($this->createElement('xsd:element', ['name' => $k, 'type' => is_array($v) ? $v['type'] : $v]));
			}
		}else{
			throw new \ErrorException("\$type should be either 'array' or 'struct'.");
		}
		$this->schema->appendChild($complexType);
	}

	/**
	 * @param string $name - operation name
	 * @param array $parameters - ['parameterName' => 'type']
	 * @param string $returnType - return type
	 */
	public function addOperation($name, array $parameters, $returnType){
		$message = $this->createElement('message', ['name' => "{$name}Request"]);
		foreach($parameters as $argument => $type){
			$message->appendChild($this->createElement('part', ['name' => $argument, 'type' => $type]));
		}
		$this->definitions->appendChild($message);
		//
		$message = $this->createElement('message', ['name' => "{$name}Response"]);
		$message->appendChild($this->createElement('part', ['name' => 'return', 'type' => $returnType]));
		$this->definitions->appendChild($message);
		//
		$operation = $this->createElement('operation', ['name' => $name]);
		$operation->appendChild($this->createElement('input', ['message' => "{$name}Request"]));
		$operation->appendChild($this->createElement('output', ['message' => "{$name}Response"]));
		$this->port->appendChild($operation);
		//
		$operation = $this->createElement('operation', ['name' => $name]);
		$operation->appendChild($this->createElement('soap:operation', ['soapAction' => $this->url.'/'.$name]));
		$operation->appendChild($this->createElement('input'))->appendChild($this->createElement('soap:body', [
			'use'           => 'encoded',
			'encodingStyle' => 'http://schemas.xmlsoap.org/soap/encoding/'
		]));
		$operation->appendChild($this->createElement('output'))->appendChild($this->createElement('soap:body', [
			'use'           => 'encoded',
			'encodingStyle' => 'http://schemas.xmlsoap.org/soap/encoding/'
		]));
		$this->binding->appendChild($operation);
	}

	/**
	 * @param null|string $outputFile
	 *
	 * @return string
	 */
	public function saveWSDL($outputFile = null){
		$this->definitions->appendChild($this->port);
		$this->definitions->appendChild($this->binding);
		// Service
		$service = $this->createElement('service', ['name' => $this->namespace]);
		$servicePort = $this->createElement('port', ['name' => $this->namespace.'Port', 'binding' => "tns:{$this->namespace}Binding"]);
		$servicePort->appendChild($this->createElement('soap:address', ['location' => $this->url]));
		$service->appendChild($servicePort);
		$this->definitions->appendChild($service);
		//
		$this->xml->formatOutput = true;
		$xml = $this->xml->saveXML();
		if(!is_null($outputFile)){
			file_put_contents($outputFile, $xml);
		}
		return $xml;
	}
}